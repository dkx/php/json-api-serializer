<?php

declare(strict_types=1);

namespace DKX\JsonApiSerializer\Resource;

use DKX\JsonApiSerializer\Relationship\ItemRelationship;

final class Collection implements DocumentDataResource
{
	/** @var Item[] */
	private $items;

	/**
	 * @param Item[] $items
	 */
	public function __construct(array $items = [])
	{
		$this->items = $items;
	}

	public function toJsonApiData(): array
	{
		return \array_map(function (Item $item) {
			return $item->toJsonApiData();
		}, $this->items);
	}

	/**
	 * @return ItemRelationship[]
	 */
	public function getIncludedItems(): array
	{
		return \array_reduce($this->items, function (array $items, Item $item) {
			return \array_merge($items, $item->getIncludedItems());
		}, []);
	}
}
