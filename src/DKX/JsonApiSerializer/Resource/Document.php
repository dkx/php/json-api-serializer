<?php

declare(strict_types=1);

namespace DKX\JsonApiSerializer\Resource;

use DKX\JsonApiSerializer\Relationship\ItemRelationship;

final class Document implements Resource
{
	/** @var DocumentDataResource|null */
	private $data;

	/** @var ErrorsList|null */
	private $errors;

	/** @var Meta|null */
	private $meta;

	public function __construct(?DocumentDataResource $data = null)
	{
		$this->data = $data;
	}

	public function setMeta(Meta $meta): void
	{
		$this->meta = $meta;
	}

	public function setErrors(ErrorsList $errorsList): void
	{
		$this->errors = $errorsList;
	}

	/**
	 * @return mixed[]
	 */
	public function toJsonApiData(): array
	{
		$doc = [];

		if ($this->data !== null) {
			$doc['data'] = $this->data->toJsonApiData();

			$included = $this->removeIncludedDuplicates(
				$this->data->getIncludedItems()
			);

			if (\count($included) > 0) {
				$doc['included'] = [];
				foreach ($included as $includedItem) {
					$doc['included'][] = $includedItem->getItem()->toJsonApiData();
				}
			}
		}

		if ($this->meta !== null) {
			$doc['meta'] = $this->meta->toJsonApiData();
		}

		if ($this->errors !== null) {
			$doc['errors'] = $this->errors->toJsonApiData();
		}

		return $doc;
	}

	/**
	 * @param ItemRelationship[] $items
	 * @return ItemRelationship[]
	 */
	protected function removeIncludedDuplicates(array $items): array
	{
		$keys = [];
		return \array_filter($items, function (ItemRelationship $relationship) use (&$keys) {
			$item = $relationship->getItem();
			$key = $item->getType(). '/'. $item->getId();

			if (\in_array($key, $keys, true)) {
				return false;
			}

			$keys[] = $key;
			return true;
		});
	}
}
