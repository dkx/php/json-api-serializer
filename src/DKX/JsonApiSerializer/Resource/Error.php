<?php

declare(strict_types=1);

namespace DKX\JsonApiSerializer\Resource;

final class Error
{
	/** @var string|null */
	private $id;

	/** @var string|null */
	private $aboutLink;

	/** @var string|null */
	private $status;

	/** @var string|null */
	private $code;

	/** @var string|null */
	private $title;

	/** @var string|null */
	private $detail;

	/** @var string|null */
	private $sourcePointer;

	/** @var string|null */
	private $sourceParameter;

	/** @var Meta|null */
	private $meta;

	public function setId(string $id): void
	{
		$this->id = $id;
	}

	public function setAboutLink(string $aboutLink): void
	{
		$this->aboutLink = $aboutLink;
	}

	public function setStatus(string $status): void
	{
		$this->status = $status;
	}

	public function setCode(string $code): void
	{
		$this->code = $code;
	}

	public function setTitle(string $title): void
	{
		$this->title = $title;
	}

	public function setDetail(string $detail): void
	{
		$this->detail = $detail;
	}

	public function setSourcePointer(string $sourcePointer): void
	{
		$this->sourcePointer = $sourcePointer;
	}

	public function setSourceParameter(string $sourceParameter): void
	{
		$this->sourceParameter = $sourceParameter;
	}

	public function setMeta(Meta $meta): void
	{
		$this->meta = $meta;
	}

	/**
	 * @return mixed[]
	 */
	public function toJsonApiData(): array
	{
		$data = [];

		if ($this->id !== null) {
			$data['id'] = $this->id;
		}

		if ($this->aboutLink !== null) {
			$data['links'] = [
				'about' => $this->aboutLink,
			];
		}

		if ($this->status !== null) {
			$data['status'] = $this->status;
		}

		if ($this->code !== null) {
			$data['code'] = $this->code;
		}

		if ($this->title !== null) {
			$data['title'] = $this->title;
		}

		if ($this->detail !== null) {
			$data['detail'] = $this->detail;
		}

		if ($this->sourcePointer !== null || $this->sourceParameter !== null) {
			$source = [];

			if ($this->sourcePointer !== null) {
				$source['pointer'] = $this->sourcePointer;
			}

			if ($this->sourceParameter !== null) {
				$source['parameter'] = $this->sourceParameter;
			}

			$data['source'] = $source;
		}

		if ($this->meta !== null) {
			$data['meta'] = $this->meta->toJsonApiData();
		}

		return $data;
	}
}
