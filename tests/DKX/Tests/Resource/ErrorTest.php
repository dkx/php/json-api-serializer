<?php

declare(strict_types=1);

namespace DKX\JsonApiSerializerTests\Resource;

use DKX\JsonApiSerializer\Resource\Error;
use DKX\JsonApiSerializer\Resource\Meta;
use PHPUnit\Framework\TestCase;

final class ErrorTest extends TestCase
{
	public function testToJsonApiData_empty(): void
	{
		$error = new Error;
		self::assertEquals([], $error->toJsonApiData());
	}

	public function testToJsonApiData(): void
	{
		$error = new Error;
		$error->setId('E123');
		$error->setAboutLink('http://localhost/error/E123');
		$error->setStatus('400');
		$error->setCode('REQUIRED');
		$error->setTitle('Title is required');
		$error->setDetail('Title field is required');
		$error->setSourcePointer('/query/title');
		$error->setSourceParameter('title');
		$error->setMeta(new Meta([
			'num' => 1,
		]));

		self::assertEquals([
			'id' => 'E123',
			'links' => [
				'about' => 'http://localhost/error/E123',
			],
			'status' => '400',
			'code' => 'REQUIRED',
			'title' => 'Title is required',
			'detail' => 'Title field is required',
			'source' => [
				'pointer' => '/query/title',
				'parameter' => 'title',
			],
			'meta' => [
				'num' => 1,
			],
		], $error->toJsonApiData());
	}
}
