<?php

declare(strict_types=1);

namespace DKX\JsonApiTests\Tests\Resource;

use DKX\JsonApiSerializer\Relationship\CollectionRelationship;
use DKX\JsonApiSerializer\Relationship\ItemRelationship;
use DKX\JsonApiSerializer\Relationship\NullRelationship;
use DKX\JsonApiSerializer\Resource\Document;
use DKX\JsonApiSerializer\Resource\Error;
use DKX\JsonApiSerializer\Resource\ErrorsList;
use DKX\JsonApiSerializer\Resource\Item;
use DKX\JsonApiSerializer\Resource\Meta;
use PHPUnit\Framework\TestCase;

final class DocumentItemTest extends TestCase
{
	public function testToJsonApiData(): void
	{
		$doc = new Document(
			new Item('book', '5', [
				Item::ATTRIBUTES => [
					'title' => 'Harry Potter',
				],
			])
		);

		self::assertEquals([
			'data' => [
				'type' => 'book',
				'id' => '5',
				'attributes' => [
					'title' => 'Harry Potter',
				],
			],
		], $doc->toJsonApiData());
	}

	public function testToJsonApiData_withSelfLink(): void
	{
		$doc = new Document(
			new Item('book', '5', [
				Item::SELF_LINK => 'http://localhost',
			])
		);

		self::assertEquals([
			'data' => [
				'type' => 'book',
				'id' => '5',
				'links' => [
					'self' => 'http://localhost',
				],
			],
		], $doc->toJsonApiData());
	}

	public function testToJsonApiData_with_null_relationship(): void
	{
		$doc = new Document(
			new Item('book', '5', [
				Item::RELATIONSHIPS => [
					'user' => new NullRelationship,
				],
			])
		);

		self::assertEquals([
			'data' => [
				'type' => 'book',
				'id' => '5',
				'relationships' => [
					'user' => [
						'data' => null,
					],
				],
			],
		], $doc->toJsonApiData());
	}

	public function testToJsonApiData_with_item_relationship(): void
	{
		$doc = new Document(
			new Item('book', '5', [
				Item::RELATIONSHIPS => [
					'user' => new ItemRelationship(
						new Item('user', '10', [
							Item::ATTRIBUTES => [
								'name' => 'John Doe',
							],
						])
					),
				],
			])
		);

		self::assertEquals([
			'data' => [
				'type' => 'book',
				'id' => '5',
				'relationships' => [
					'user' => [
						'data' => [
							'type' => 'user',
							'id' => '10',
						],
					],
				],
			],
			'included' => [
				[
					'type' => 'user',
					'id' => '10',
					'attributes' => [
						'name' => 'John Doe',
					],
				],
			],
		], $doc->toJsonApiData());
	}

	public function testToJsonApiData_with_nested_item_relationship(): void
	{
		$doc = new Document(
			new Item('book', '5', [
				Item::RELATIONSHIPS => [
					'user' => new ItemRelationship(
						new Item('user', '10', [
							Item::ATTRIBUTES => [
								'name' => 'John Doe',
							],
							Item::RELATIONSHIPS => [
								'role' => new ItemRelationship(
									new Item('role', 'admin', [
										Item::ATTRIBUTES => [
											'title' => 'Admin',
										],
									])
								),
							],
						])
					),
				],
			])
		);

		self::assertEquals([
			'data' => [
				'type' => 'book',
				'id' => '5',
				'relationships' => [
					'user' => [
						'data' => [
							'type' => 'user',
							'id' => '10',
						],
					],
				],
			],
			'included' => [
				[
					'type' => 'role',
					'id' => 'admin',
					'attributes' => [
						'title' => 'Admin',
					],
				],
				[
					'type' => 'user',
					'id' => '10',
					'attributes' => [
						'name' => 'John Doe',
					],
					'relationships' => [
						'role' => [
							'data' => [
								'type' => 'role',
								'id' => 'admin',
							],
						],
					],
				],
			],
		], $doc->toJsonApiData());
	}

	public function testToJsonApiData_with_collection_relationship(): void
	{
		$doc = new Document(
			new Item('book', '5', [
				Item::RELATIONSHIPS => [
					'users' => new CollectionRelationship([
						new ItemRelationship(
							new Item('user', '10', [
								Item::ATTRIBUTES => [
									'name' => 'John Doe',
								],
							])
						),
						new ItemRelationship(
							new Item('user', '20', [
								Item::ATTRIBUTES => [
									'name' => 'Lord Voldemort',
								],
							])
						),
					]),
				],
			])
		);

		self::assertEquals([
			'data' => [
				'type' => 'book',
				'id' => '5',
				'relationships' => [
					'users' => [
						'data' => [
							[
								'type' => 'user',
								'id' => '10',
							],
							[
								'type' => 'user',
								'id' => '20',
							],
						],
					],
				],
			],
			'included' => [
				[
					'type' => 'user',
					'id' => '10',
					'attributes' => [
						'name' => 'John Doe',
					],
				],
				[
					'type' => 'user',
					'id' => '20',
					'attributes' => [
						'name' => 'Lord Voldemort',
					],
				],
			],
		], $doc->toJsonApiData());
	}

	public function testToJsonApiData_with_duplicate_collection_relationship(): void
	{
		$doc = new Document(
			new Item('book', '5', [
				Item::RELATIONSHIPS => [
					'users' => new CollectionRelationship([
						new ItemRelationship(
							new Item('user', '10', [
								Item::ATTRIBUTES => [
									'name' => 'John Doe',
								],
								Item::RELATIONSHIPS => [
									'role' => new ItemRelationship(
										new Item('role', 'admin', [
											Item::ATTRIBUTES => [
												'title' => 'Admin',
											],
										])
									),
								],
							])
						),
						new ItemRelationship(
							new Item('user', '20', [
								Item::ATTRIBUTES => [
									'name' => 'Lord Voldemort',
								],
								Item::RELATIONSHIPS => [
									'role' => new ItemRelationship(
										new Item('role', 'admin', [
											'title' => 'Admin',
										])
									),
								],
							])
						),
					]),
				],
			])
		);

		self::assertEquals([
			'data' => [
				'type' => 'book',
				'id' => '5',
				'relationships' => [
					'users' => [
						'data' => [
							[
								'type' => 'user',
								'id' => '10',
							],
							[
								'type' => 'user',
								'id' => '20',
							],
						],
					],
				],
			],
			'included' => [
				[
					'type' => 'role',
					'id' => 'admin',
					'attributes' => [
						'title' => 'Admin',
					],
				],
				[
					'type' => 'user',
					'id' => '10',
					'attributes' => [
						'name' => 'John Doe',
					],
					'relationships' => [
						'role' => [
							'data' => [
								'type' => 'role',
								'id' => 'admin',
							],
						],
					],
				],
				[
					'type' => 'user',
					'id' => '20',
					'attributes' => [
						'name' => 'Lord Voldemort',
					],
					'relationships' => [
						'role' => [
							'data' => [
								'type' => 'role',
								'id' => 'admin',
							],
						],
					],
				],
			],
		], $doc->toJsonApiData());
	}

	public function testToJsonApiData_meta(): void
	{
		$doc = new Document;
		$doc->setMeta(new Meta([
			'authenticated' => true,
		]));

		self::assertEquals([
			'meta' => [
				'authenticated' => true,
			],
		], $doc->toJsonApiData());
	}

	public function testToJsonApiData_errors(): void
	{
		$error = new Error;
		$error->setCode('REQUIRED');

		$doc = new Document;
		$doc->setErrors(new ErrorsList([$error]));

		self::assertEquals([
			'errors' => [
				[
					'code' => 'REQUIRED',
				],
			],
		], $doc->toJsonApiData());
	}
}
