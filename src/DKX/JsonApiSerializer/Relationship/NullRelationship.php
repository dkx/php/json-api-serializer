<?php

declare(strict_types=1);

namespace DKX\JsonApiSerializer\Relationship;

final class NullRelationship implements Relationship
{
	public function toJsonApiRelationshipData(): ?array
	{
		return null;
	}

	/**
	 * @return ItemRelationship[]
	 */
	public function getIncludedItems(): array
	{
		return [];
	}
}
