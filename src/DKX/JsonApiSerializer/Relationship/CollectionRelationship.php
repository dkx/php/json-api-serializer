<?php

declare(strict_types=1);

namespace DKX\JsonApiSerializer\Relationship;

final class CollectionRelationship implements Relationship
{
	/** @var ItemRelationship[] */
	private $items;

	/**
	 * @param ItemRelationship[] $items
	 */
	public function __construct(array $items = [])
	{
		$this->items = $items;
	}

	/**
	 * @return mixed[]
	 */
	public function toJsonApiRelationshipData(): array
	{
		return \array_map(function (ItemRelationship $item) {
			return $item->toJsonApiRelationshipData();
		}, $this->items);
	}

	/**
	 * @return ItemRelationship[]
	 */
	public function getIncludedItems(): array
	{
		return \array_reduce($this->items, function (array $items, ItemRelationship $item) {
			return \array_merge($items, $item->getIncludedItems());
		}, []);
	}
}
