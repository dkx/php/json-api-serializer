<?php

declare(strict_types=1);

namespace DKX\JsonApiTests\Tests\Resource;

use DKX\JsonApiSerializer\Resource\Document;
use DKX\JsonApiSerializer\Resource\NullResource;
use PHPUnit\Framework\TestCase;

final class DocumentNullTest extends TestCase
{
	public function testToJsonApiDataNull(): void
	{
		$doc = new Document(
			new NullResource,
		);

		self::assertEquals(['data' => null], $doc->toJsonApiData());
	}
}
