<?php

declare(strict_types=1);

namespace DKX\JsonApiSerializer\Resource;

final class Meta
{
	/** @var mixed[] */
	private $meta;

	/**
	 * @param mixed[] $meta
	 */
	public function __construct(array $meta)
	{
		$this->meta = $meta;
	}

	/**
	 * @return mixed[]
	 */
	public function toJsonApiData(): array
	{
		return $this->meta;
	}
}
