<?php

declare(strict_types=1);

namespace DKX\JsonApiSerializerTests\Resource;

use DKX\JsonApiSerializer\Resource\Document;
use PHPUnit\Framework\TestCase;

final class DocumentEmptyTest extends TestCase
{
	public function testToJsonApiData(): void
	{
		$doc = new Document;
		self::assertEquals([], $doc->toJsonApiData());
	}
}
