<?php

declare(strict_types=1);

namespace DKX\JsonApiTests\Tests\Resource;

use DKX\JsonApiSerializer\Relationship\CollectionRelationship;
use DKX\JsonApiSerializer\Relationship\ItemRelationship;
use DKX\JsonApiSerializer\Relationship\NullRelationship;
use DKX\JsonApiSerializer\Resource\Collection;
use DKX\JsonApiSerializer\Resource\Document;
use DKX\JsonApiSerializer\Resource\Item;
use PHPUnit\Framework\TestCase;

final class DocumentCollectionTest extends TestCase
{
	public function testToJsonApiData(): void
	{
		$doc = new Document(
			new Collection([
				new Item('book', '5', [
					Item::ATTRIBUTES => [
						'title' => 'Harry Potter',
					],
				]),
			])
		);

		self::assertEquals([
			'data' => [
				[
					'type' => 'book',
					'id' => '5',
					'attributes' => [
						'title' => 'Harry Potter',
					],
				]
			],
		], $doc->toJsonApiData());
	}

	public function testToJsonApiData_with_null_relationship(): void
	{
		$doc = new Document(
			new Collection([
				new Item('book', '5', [
					Item::RELATIONSHIPS => [
						'user' => new NullRelationship,
					],
				]),
			])
		);

		self::assertEquals([
			'data' => [
				[
					'type' => 'book',
					'id' => '5',
					'relationships' => [
						'user' => [
							'data' => null,
						],
					],
				],
			],
		], $doc->toJsonApiData());
	}

	public function testToJsonApiData_with_item_relationship(): void
	{
		$doc = new Document(
			new Collection([
				new Item('book', '5', [
					Item::RELATIONSHIPS => [
						'user' => new ItemRelationship(
							new Item('user', '10', [
								Item::ATTRIBUTES => [
									'name' => 'John Doe',
								],
							])
						),
					],
				]),
			])
		);

		self::assertEquals([
			'data' => [
				[
					'type' => 'book',
					'id' => '5',
					'relationships' => [
						'user' => [
							'data' => [
								'type' => 'user',
								'id' => '10',
							],
						],
					],
				],
			],
			'included' => [
				[
					'type' => 'user',
					'id' => '10',
					'attributes' => [
						'name' => 'John Doe',
					],
				],
			],
		], $doc->toJsonApiData());
	}

	public function testToJsonApiData_with_nested_item_relationship(): void
	{
		$doc = new Document(
			new Collection([
				new Item('book', '5', [
					Item::RELATIONSHIPS => [
						'user' => new ItemRelationship(
							new Item('user', '10', [
								Item::ATTRIBUTES => [
									'name' => 'John Doe',
								],
								Item::RELATIONSHIPS => [
									'role' => new ItemRelationship(
										new Item('role', 'admin', [
											Item::ATTRIBUTES => [
												'title' => 'Admin',
											],
										])
									),
								],
							])
						),
					],
				]),
			])
		);

		self::assertEquals([
			'data' => [
				[
					'type' => 'book',
					'id' => '5',
					'relationships' => [
						'user' => [
							'data' => [
								'type' => 'user',
								'id' => '10',
							],
						],
					],
				],
			],
			'included' => [
				[
					'type' => 'role',
					'id' => 'admin',
					'attributes' => [
						'title' => 'Admin',
					],
				],
				[
					'type' => 'user',
					'id' => '10',
					'attributes' => [
						'name' => 'John Doe',
					],
					'relationships' => [
						'role' => [
							'data' => [
								'type' => 'role',
								'id' => 'admin',
							],
						],
					],
				],
			],
		], $doc->toJsonApiData());
	}

	public function testToJsonApiData_with_item_relationship_duplicates(): void
	{
		$user = new ItemRelationship(
			new Item('user', '10', [
				Item::ATTRIBUTES => [
					'name' => 'John Doe',
				],
			])
		);

		$doc = new Document(
			new Collection([
				new Item('book', '5', [
					Item::RELATIONSHIPS => [
						'user' => $user,
					],
				]),
				new Item('book', '15', [
					Item::RELATIONSHIPS => [
						'user' => $user,
					],
				]),
			])
		);

		self::assertEquals([
			'data' => [
				[
					'type' => 'book',
					'id' => '5',
					'relationships' => [
						'user' => [
							'data' => [
								'type' => 'user',
								'id' => '10',
							],
						],
					],
				],
				[
					'type' => 'book',
					'id' => '15',
					'relationships' => [
						'user' => [
							'data' => [
								'type' => 'user',
								'id' => '10',
							],
						],
					],
				],
			],
			'included' => [
				[
					'type' => 'user',
					'id' => '10',
					'attributes' => [
						'name' => 'John Doe',
					],
				],
			],
		], $doc->toJsonApiData());
	}

	public function testToJsonApiData_with_collection_relationship(): void
	{
		$doc = new Document(
			new Collection([
				new Item('book', '5', [
					Item::RELATIONSHIPS => [
						'users' => new CollectionRelationship([
							new ItemRelationship(
								new Item('user', '10', [
									Item::ATTRIBUTES => [
										'name' => 'John Doe',
									],
								])
							),
						]),
					],
				]),
			])
		);

		self::assertEquals([
			'data' => [
				[
					'type' => 'book',
					'id' => '5',
					'relationships' => [
						'users' => [
							'data' => [
								[
									'type' => 'user',
									'id' => '10',
								],
							],
						],
					],
				],
			],
			'included' => [
				[
					'type' => 'user',
					'id' => '10',
					'attributes' => [
						'name' => 'John Doe',
					],
				],
			],
		], $doc->toJsonApiData());
	}
}
