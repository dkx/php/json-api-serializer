<?php

declare(strict_types=1);

namespace DKX\JsonApiSerializer\Resource;

use DKX\JsonApiSerializer\Relationship\ItemRelationship;
use DKX\JsonApiSerializer\Relationship\Relationship;

final class Item implements DocumentDataResource
{
	public const ATTRIBUTES = 'attributes';

	public const RELATIONSHIPS = 'relationships';

	public const SELF_LINK = 'self_link';

	/** @var string */
	protected $type;

	/** @var string */
	protected $id;

	/** @var mixed[] */
	private $attributes;

	/** @var Relationship[] */
	private $relationships;

	/** @var string|null */
	private $selfLink;

	/**
	 * @param mixed[] $data
	 */
	public function __construct(string $type, string $id, array $data = [])
	{
		$this->type = $type;
		$this->id = $id;
		$this->attributes = $data[self::ATTRIBUTES] ?? [];
		$this->relationships = $data[self::RELATIONSHIPS] ?? [];
		$this->selfLink = $data[self::SELF_LINK] ?? null;
	}

	public function getType(): string
	{
		return $this->type;
	}

	public function getId(): string
	{
		return $this->id;
	}

	public function toJsonApiData(): array
	{
		$data = [
			'type' => $this->type,
			'id' => $this->id,
		];

		if (count($this->attributes) > 0) {
			$data['attributes'] = $this->attributes;
		}

		if (count($this->relationships) > 0) {
			$data['relationships'] = [];
			foreach ($this->relationships as $name => $relationship) {
				$data['relationships'][$name] = [
					'data' => $relationship->toJsonApiRelationshipData(),
				];
			}
		}

		if ($this->selfLink !== null) {
			$data['links'] = [
				'self' => $this->selfLink,
			];
		}

		return $data;
	}

	/**
	 * @return ItemRelationship[]
	 */
	public function getIncludedItems(): array
	{
		return \array_reduce($this->relationships, function (array $items, Relationship $relationship) {
			return \array_merge($items, $relationship->getIncludedItems());
		}, []);
	}
}
