<?php

declare(strict_types=1);

namespace DKX\JsonApiSerializer;

use DKX\JsonApiSerializer\Resource\Document;

final class Serializer
{
	/**
	 * @return mixed[]
	 */
	public function serialize(Document $document): array
	{
		return $document->toJsonApiData();
	}
}
