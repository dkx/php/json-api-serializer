<?php

declare(strict_types=1);

namespace DKX\JsonApiSerializer\Resource;

final class ErrorsList
{
	/** @var Error[] */
	private $errors;

	/**
	 * @param Error[] $errors
	 */
	public function __construct(array $errors)
	{
		$this->errors = $errors;
	}

	/**
	 * @return mixed[]
	 */
	public function toJsonApiData(): array
	{
		return \array_map(function (Error $error) {
			return $error->toJsonApiData();
		}, $this->errors);
	}
}
