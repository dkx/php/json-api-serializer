<?php

declare(strict_types=1);

namespace DKX\JsonApiSerializer\Relationship;

interface Relationship
{
	/**
	 * @return mixed[]|null
	 */
	public function toJsonApiRelationshipData(): ?array;

	/**
	 * @return ItemRelationship[]
	 */
	public function getIncludedItems(): array;
}
