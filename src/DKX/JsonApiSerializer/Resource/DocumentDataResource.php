<?php

declare(strict_types=1);

namespace DKX\JsonApiSerializer\Resource;

use DKX\JsonApiSerializer\Relationship\ItemRelationship;

interface DocumentDataResource extends Resource
{
	/**
	 * @return ItemRelationship[]
	 */
	public function getIncludedItems(): array;
}
