<?php

declare(strict_types=1);

namespace DKX\JsonApiSerializerTests\Resource;

use DKX\JsonApiSerializer\Resource\Error;
use DKX\JsonApiSerializer\Resource\ErrorsList;
use PHPUnit\Framework\TestCase;

final class ErrorsListTest extends TestCase
{
	public function testToJsonApiData(): void
	{
		$error = new Error;
		$error->setCode('REQUIRED');
		$errors = new ErrorsList([$error]);

		self::assertEquals([
			[
				'code' => 'REQUIRED',
			],
		], $errors->toJsonApiData());
	}
}
