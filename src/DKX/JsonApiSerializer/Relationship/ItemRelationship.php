<?php

declare(strict_types=1);

namespace DKX\JsonApiSerializer\Relationship;

use DKX\JsonApiSerializer\Resource\Item;

final class ItemRelationship implements Relationship
{
	/** @var Item */
	private $item;

	public function __construct(Item $item)
	{
		$this->item = $item;
	}

	public function getItem(): Item
	{
		return $this->item;
	}

	/**
	 * @return mixed[]
	 */
	public function toJsonApiRelationshipData(): array
	{
		return [
			'type' => $this->item->getType(),
			'id' => $this->item->getId(),
		];
	}

	/**
	 * @return ItemRelationship[]
	 */
	public function getIncludedItems(): array
	{
		return \array_merge($this->item->getIncludedItems(), [$this]);
	}
}
