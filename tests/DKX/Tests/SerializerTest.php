<?php

declare(strict_types=1);

namespace DKX\JsonApiSerializerTests;

use DKX\JsonApiSerializer\Resource\Document;
use DKX\JsonApiSerializer\Serializer;
use PHPUnit\Framework\TestCase;

final class SerializerTest extends TestCase
{
	public function testSerialize(): void
	{
		$serializer = new Serializer;
		$doc = new Document;

		self::assertEquals([], $serializer->serialize($doc));
	}
}
