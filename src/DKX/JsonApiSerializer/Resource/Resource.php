<?php

declare(strict_types=1);

namespace DKX\JsonApiSerializer\Resource;

interface Resource
{
	/**
	 * @return mixed[]|null
	 */
	public function toJsonApiData(): ?array;
}
