<?php

declare(strict_types=1);

namespace DKX\JsonApiSerializer\Resource;

use DKX\JsonApiSerializer\Relationship\ItemRelationship;

final class NullResource implements DocumentDataResource
{
	/**
	 * @return mixed[]|null
	 */
	public function toJsonApiData(): ?array
	{
		return null;
	}

	/**
	 * @return ItemRelationship[]
	 */
	public function getIncludedItems(): array
	{
		return [];
	}
}
